from django.db import models

# Create your models here.
class User(models.Model):
    json = models.TextField(default="{}")
    number = models.IntegerField()

class Route(models.Model):
    json = models.TextField(default="{}")
    name = models.TextField(default="")
