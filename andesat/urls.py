from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^get_json$', 'world.views.get_json', name='home'),
    url(r'^service$', 'world.views.set_json', name='home'),
    url(r'^delete_db$', 'world.views.delete_all_db', name='home'),
    # url(r'^andesat/', include('andesat.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
