# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from world.models import User, Route
import logging

try:
    import simplejson as json
except ImportError:
    import json

class LazyEncoder(json.JSONEncoder):
    """Encodes django's lazy i18n strings.
    Used to serialize translated strings to JSON, because
    simplejson chokes on it otherwise.
    """
    def default(self, obj):
        if isinstance(obj, Promise):
            return force_unicode(obj)
        return obj

class Requester:

    def __init__(self):
        self.log = logging.getLogger("Requester")
        hdlr = logging.FileHandler('Requester.log')
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        self.log.addHandler(hdlr)

    def log_request(self, request):
        self.log.info(request)


def get_json(request):
    return HttpResponse(json.dumps(json.loads(open('/home/ubuntu/json').read()), cls=LazyEncoder), content_type="application/json")


@csrf_exempt
def set_json(request):
    post_data = dict(request.POST.iteritems())
    if "type" in post_data:
        if post_data['type'] == 'set':
            contents = json.loads(post_data['contents'])
            user_contents = contents['contents']['nums'][0]
            the_user, created = User.objects.get_or_create(
                number = user_contents['num']
            )

            #if not created:
	    the_user.json = json.dumps(user_contents)
            the_user.save()

	    route_contents = contents['contents']['route']
	    the_route, created = Route.objects.get_or_create(
                name = route_contents['route_name']
            )

	    the_route.json = json.dumps(route_contents)
            the_route.save()


        elif post_data['type'] == 'get':
	    routes = Route.objects.all()
            users = User.objects.all()
            nums = []
            for user in users:
                user_data = json.loads(user.json)
                nums.append(user_data)
	    post_data['contents'] = {'nums': nums}
	    for route in routes:		
 	        post_data['contents']['route'] = json.loads(route.json)

            return HttpResponse(json.dumps(post_data), content_type="application/json")

    return HttpResponse(json.dumps({
        "$result_set": request.body
    }), content_type="application/json")


@csrf_exempt
def delete_all_db(request):
    User.objects.all().delete()
    Route.objects.all().delete()
    routes = Route.objects.all()
    users = User.objects.all()
    return HttpResponse(json.dumps({
        "$result_set": {'users': len(users), 'routes': len(routes)}
    }), content_type="application/json")
